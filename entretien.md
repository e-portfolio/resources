# Compte-rendu de la réunion du 16 Janvier 2017

## Travail effectué : 

* Lecture de l'article "Towards Personal infrastucture to manage long term open learn models". Tour du problèmes et des enjeux bien présenté, et semble bien complet. On comprends bien l'intérêt de la solution basé sur CozyCloud. J'ai trouvé très intéressant les 4 fonctionnalités nécessaires pour faire du « data management » (Data access, data duration, personal data storage, data transfer)

* Recherche personnelle sur les portfolio :
	* [Comparaison d'outils de portfolio](http://electronicportfolios.com/myportfolio/versions.html)
	* [La musette du travailleur](http://www.fing.org/IMG/pdf/Dossier-partenaires-Musette_janvier2016.pdf)
	* [Analyse du concept de la musette](http://www.usine-digitale.fr/article/face-aux-parcours-de-plus-en-plus-discontinus-capitalisons-nos-acquis-dans-une-musette.N392212)
	* [Dossier sur le portfolio réalisé par Eduscol (ministère de l'Éducation nationale)](http://eduscol.education.fr/numerique/dossier/archives/portfolionumerique)
	* [Dossier réalisé par la CLIC (Québec) sur le portfolio](http://clic.ntic.org/cgi-bin/aff.pl?page=article&id=2053) : Présentation de trois aspects fonctionnels des portfolio (apprentissage, présentation, évaluation)
	
* Recherche de solutions d'e-portfolio : 
    * [liste d'outil à dégrossir](http://mashable.com/2013/09/17/online-portfolio/#BA15zw4sOOqU)
    * [liste d'outil 2, un peu plus intéressant](https://eportfolioreview.wordpress.com/eportfolio-list/)
 
* Test avorté du prototype de Cédric

## Entretien
* Cible de l'apprenant pour le portfolio :
    * utilisateur moyennement technique (tourné sur l'IT sans être ingénieur)
    * équivalent d'un étudiant en master (du vécu en tant qu'étudiant, et possède un projet professionnel)
    
* Objectif de rencontrer un sociologue de l'éducation pour voir avec lui les fonctionnalités nécessaire pour pouvoir travailler avec des étudiants (futurs professeurs)
    * voir Mercredi 18 Janvier pour fixer un RDV
    
* Discussion sur les lacunes des solutions comme Mahara : 
    * Problèmes techniques -> difficile à administrer et à configurer
    * Reste "enfermé" dans une école ou université. L'étudiant n'a pas la main sur ses propres données (le problème est général et n'est pas propre à Mahara. Les données sur le parcours scolaire d'un étudiant sont conservés en moyenne 5 ans en France. Peu de choses claires pour pouvoir récupérer ces données)
    
* Utiliser un portfolio sur une instance (gratuite ou payante) chez Cozcyloud revient à utiliser un service de portofolio "centralisé" :
    * CozyCloud fait le pari que l'auto-hébergement, ou le fait de louer un serveur chez un hébergeur sera un comportement un peu plus généralisé. Le pari consiste à penser que les utilisateurs vont prendre conscience de l'intérêt et l'importance d'avoir un cloud "personnel". 
    * Rends important la question de l'export des données (si l'utilisateur souhaite changer d'outil, par exemple)
    
## A faire
* Se familiariser avec l'environnement Cozy Cloud pour pouvoir tester la solution de Cédric

* Transmettre le rapport de stage de fin d'études (pour jeter un oeil sur mes capacités rédactionnelles)

* Recherche sur l'existance de modèles de données pour pouvoir exporter facilement ses données. Question de l'ontologie des données d'un portfolio. 

* Rédaction d'un rapport bibliographique qui contiendra : 
    * Étude de l'existant (analysé selon les fonctionnalités nécessaires au "data management" et selon les trois aspects (apprentissage, présentation, évaluation) d'un portfolio
    * Faire de même pour la solution de Cédric
    * Rapide présentation de l'environnement technique
    * Le rapport devra être bien structuré, et bien sourcé. 
